const fs = require("fs");
const path = require("path");
const svgexport = require("svgexport");
const yargs = require("yargs");

const inputPath = path.join(__dirname, "input");
const outputPath = path.join(__dirname, "output");

const argv = yargs
  .option("extension", {
    alias: "e",
    description: "Specify output type ('png' or 'jpg').",
    default: "png",
    type: "string"
  })
  .option("options", {
    alias: "o",
    description: "Override svgexport options",
    type: "array"
  })
  .help()
  .alias("help", "h")
  .argv;

fs.readdir(inputPath, (err, files) => {
  let data = [];

  files.forEach(file => {
    const extension = path.extname(file);
    const name = path.basename(file, extension);

    const inputFile = path.join(inputPath, file);
    const outputFile = path.join(outputPath, file);

    if (!fs.existsSync(outputFile) && extension === ".svg") {
      data.push({
        "input": [inputFile, argv.extension],
        "output": [path.join(outputPath, name + "." + argv.extension), ...(argv.options ? options : [])]
      });
    }
  });

  svgexport.render(data);
});
