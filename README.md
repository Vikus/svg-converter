# SVG converter

## Prerequisites

Compatible with Linux, Mac or WSL. For headless usage in Linux you need required packages for puppeteer, eg. in Debian at least minimal install of xfce4 and maybe some other libraries (`sudo apt install xfce4 --no-install-recommends`).

## Setup and usage

1. Install dependencies with `yarn install` (or with npm).
2. Copy files to `./input` directory.
3. Run `yarn run svg-converter`.
4. Processed files will be created in `./output` directory.
